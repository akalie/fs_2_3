<?php

/*
Задача 3
Дан текстовый файл размером 2ГБ. Напишите класс, реализующий интерфейс SeekableIterator, для чтения данного файла.
*/

/**
 * 2 варант, чтение по символам
 *
 * Поддерживает несколько кодировок с фиксированной длинной символа и utf-8
 * - решение на коленке, "полотенцевое"
 * - очень медленная операция seek() для utf-8
 * - не успел написать тесты(
 */


class SeekableIteratorImplementation2 implements SeekableIterator
{
    const VARIABLE_LENGTH = '~';

    private $encodingToCharLength = [
        'UTF-32' => 4,
        'ISO-8859-1' => 1,
        'Windows-1252' => 1,
        'Windows-1251' => 1,
        'UTF-8' => self::VARIABLE_LENGTH,
    ];

    private $fileHandler;
    private $currentElement;
    private $currentPosition = 0;
    private $fileSize;
    private $charLength;
    private $fileCharSize;

    public function __construct($filePath, $encoding)
    {
        if (!file($filePath)) {
            throw new InvalidArgumentException('No such file');
        }

        if (!isset($this->encodingToCharLength[$encoding])) {
            throw new InvalidArgumentException('Unsupported encoding');
        }

        $this->fileHandler = fopen($filePath, 'rb');
        $this->fileSize = filesize($filePath);
        $this->charLength = $this->encodingToCharLength[$encoding];
    }

    /**
     * Return the current element
     */
    public function current()
    {
        if ($this->currentElement === null) {
            $this->currentElement = $this->readChar();
        }
        return $this->currentElement;
    }

    /**
     * Move forward to next element until EOF
     */
    public function next()
    {
        $newCurrentValue = $this->readChar();

        if ($newCurrentValue === false) {
            $this->fileCharSize = $this->currentPosition;
        }

        $this->currentPosition++;
        $this->currentElement = $newCurrentValue;
    }

    private function readChar()
    {
        if ($this->charLength !== self::VARIABLE_LENGTH) {
            return  $this->getNextCharWithCharLength();
        }

        return $this->getNextCharUtf8();
    }

    private function getNextCharWithCharLength()
    {
        $temporaryValue = '';
        $charLength = $this->charLength;
        while ($charLength--) {
            $char = fgetc($this->fileHandler);
            // конец файла
            if ($char === false) {
                return false;
            }
            $temporaryValue .= $char;
        }

        return $temporaryValue;
    }

    private function getNextCharUtf8()
    {
        $char = fgetc($this->fileHandler);
        // конец файла
        if ($char === false) {
            return false;
        }
        $temporaryValue = $char;

        $bin = ord($char);
        if ($bin >= 240) {
            $charLength = 4;
        } elseif ($bin >= 224) {
            $charLength = 3;
        } elseif ($bin >= 192) {
            $charLength = 2;
        } else {
            return $char;
        }

        while (--$charLength) {
            $char = fgetc($this->fileHandler);
            $temporaryValue .= $char;
        }

        return $temporaryValue;
    }

    /**
     * Return the key of the current element
     */
    public function key()
    {
        return $this->currentPosition;
    }

    /**
     * Checks if current position is valid
     */
    public function valid()
    {
        if ($this->charLength !== self::VARIABLE_LENGTH) {
            return $this->fileSize > $this->charLength * $this->currentPosition;
        }

        if ($this->fileCharSize) {
            return $this->fileCharSize >= $this->currentPosition;
        }

        // так как у нас нет возможности засетить fileCharSize для utf-8 без промотки всего файла до конца
        return true;
    }

    /**
     * Rewind the Iterator to the first element
     */
    public function rewind()
    {
        $this->currentPosition = 0;
        $this->currentElement = null;
        rewind($this->fileHandler);
    }

    /**
     * Seeks to a position
     */
    public function seek($position)
    {
        if ($this->charLength !== self::VARIABLE_LENGTH) {
            fseek($this->fileHandler, $this->charLength * $position);
            $this->currentElement = $this->current();
            $this->currentPosition = (int) $position;
        } else {
            if ($position < $this->currentPosition) {
                $this->rewind();
            }

            while ($this->currentPosition !== (int) $position) {
                $this->next();
                $this->currentElement = null;
            }
        }
    }
}
