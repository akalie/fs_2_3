<?php

/*
 * Задача 2
Имеется таблица пользователей:

CREATE TABLE `users` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`name` varchar(32) NOT NULL,
`gender` tinyint(2) NOT NULL,
`email` varchar(1024) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB;

В таблице более 100 млн записей, и она находится под нагрузкой в production (идут запросы на добавление / изменение / удаление).
В поле email может быть от одного до нескольких перечисленных через запятую адресов. Может быть пусто.
Напишите скрипт, который выведет список представленных в таблице почтовых доменов с количеством пользователей по каждому домену.

 */

/**
 * Readme не очень быстро, без хитрых запросов, но очень слабо грузит систему - тяжелых запросов тоже нет.
 * На 3 млн записей на локальной машине не заметил прироста cpu usage, заняло ~30сек
 * Минусы - может быть затратно по памяти (итоговый массив может разростись), нет обработки ошибок (быстроскрипт), некрасиво
 * 1 можно решить, например, редисом
 */

$conn = new PDO('mysql:dbname=fs2;host=localhost;port=3306', 'bbb', 'ppp');

$chunkSize = 100000;

print_r(getDomainToUsers($conn, $chunkSize));

function getDomainToUsers(\PDO $conn, int $chunkSize): array
{
    $domainToUsersCount = [];
    $to = 0;
    $maxId = 1;

    while ($to < $maxId) {
        $from = $to;
        $to += $chunkSize;

        $emails = getEmailsChunk($conn, $from, $to);
        if ($emails) {
            processEmails($domainToUsersCount, $emails);
        }

        $maxId = getMaxUserId($conn);

        usleep(100000);
    }

    return $domainToUsersCount;
}

function getMaxUserId(\PDO $conn): int
{
    $stmt = $conn->query('SELECT MAX(`id`) FROM `users`');
    return (int) $stmt->fetchColumn();
}

function getEmailsChunk(\Pdo $conn, int $idFrom, int $idTo)
{
    if ($idFrom > $idTo) {
        throw new InvalidArgumentException('idFrom should be greater than idTo');
    }
    $stmt = $conn->query("SELECT `email` FROM `users` WHERE id BETWEEN $idFrom  AND $idTo");
    echo "SELECT `email` FROM `users` WHERE id BETWEEN $idFrom  AND $idTo" . PHP_EOL;
    return $stmt->fetchAll(PDO::FETCH_COLUMN);
}

function processEmails(&$domainToUsersCount, $emails)
{
    foreach ($emails as $emailsString) {
        if (!$emailsString) continue;
        $tmp = explode(',', $emailsString);
        foreach ($tmp as $email) {
            $atPos = strpos($email, '@');
            if (!$atPos) continue;

            $domain = substr($email, $atPos);

            if (isset($domainToUsersCount[$domain])) {
                $domainToUsersCount[$domain]++;
            } else {
                $domainToUsersCount[$domain] = 1;
            }
        }
    }
}