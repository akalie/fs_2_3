<?php

/*
Задача 3
Дан текстовый файл размером 2ГБ. Напишите класс, реализующий интерфейс SeekableIterator, для чтения данного файла.
*/

/**
 * 1 варант, чтение по линиям
 */


class SeekableIteratorImplementation implements SeekableIterator
{
    private $decorated;

    public function __construct(string $filePath)
    {
        if (!is_file($filePath)) {
            throw new InvalidArgumentException('No such file');
        }
        $fileSize = filesize($filePath);

        // Согласно ГОСТ 8.417—2002 термин гигабайт с обозначением «ГБ» равен 1 000 000 000
        // https://ru.wikipedia.org/wiki/%D0%93%D0%B8%D0%B3%D0%B0%D0%B1%D0%B0%D0%B9%D1%82
        if ($fileSize !== 2 * (10 ** 9)) {
            throw new InvalidArgumentException('Incorrect file size');
        }

        $this->decorated = new SplFileObject($filePath, 'rb');
    }

    /**
     * Return the current element
     */
    public function current()
    {
        return $this->decorated->current();
    }

    /**
     * Move forward to next element
     */
    public function next()
    {
        $this->decorated->next();
    }

    /**
     * Return the key of the current element
     */
    public function key()
    {
        return $this->decorated->key();
    }

    /**
     * Checks if current position is valid
     */
    public function valid()
    {
        return $this->decorated->valid();
    }

    /**
     * Rewind the Iterator to the first element
     */
    public function rewind()
    {
        $this->decorated->rewind();
    }

    /**
     * Seeks to a position
     */
    public function seek($position)
    {
        $this->decorated->seek($position);
    }
}
